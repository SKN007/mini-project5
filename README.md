# Mini projrct 5

## Requiements

Create a Rust AWS Lambda function (or app runner)

Implement a simple service

Connect to a database

## Steps

### Environment setup

1. Create a rust lambda function
   ```
   cargo lambda new project5
   ```
2. Create a table in DynamoDB called "project5", and name the Partition key "accessId".
3. Write code in src/main.rs. The function of this lambda function is to add an item to the table "project5, which contains the access time and access IP (randomly generated)".
   ```
   use aws_config::meta::region::RegionProviderChain;
   use aws_sdk_dynamodb::{Client, model::AttributeValue};
   use chrono::{DateTime, Utc};
   use chrono_tz::America::New_York;
   use lambda_runtime::{Context, Error as LambdaError, LambdaEvent, service_fn};
   use rand::thread_rng;
   use rand::Rng;
   use serde_json::{json, Value};
   use std::collections::HashMap;
   use uuid::Uuid;
   use tokio;

   async fn insert_access_record(dynamodb_client: &Client, table_name: &str) -> Result<(), aws_sdk_dynamodb::Error> {
       let access_id = Uuid::new_v4().to_string();
       let utc_now: DateTime<Utc> = Utc::now();
       let eastern_time = utc_now.with_timezone(&New_York).to_rfc3339();
       let ip_address = generate_random_ip();

       let mut item = HashMap::new();
       item.insert("accessId".to_string(), AttributeValue::S(access_id));
       item.insert("accessTime".to_string(), AttributeValue::S(eastern_time));
       item.insert("ipAddress".to_string(), AttributeValue::S(ip_address));

       dynamodb_client.put_item()
           .table_name(table_name)
           .set_item(Some(item))
           .send()
           .await?;

       Ok(())
   }

   fn generate_random_ip() -> String {
       let mut rng = thread_rng();
       format!("{}.{}.{}.{}", rng.gen_range(0..256), rng.gen_range(0..256), rng.gen_range(0..256), rng.gen_range(0..256))
   }

   async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
       let _event = event.payload; // If you need to use the event
       let region_provider = RegionProviderChain::default_provider().or_else("us-east-1");
       let shared_config = aws_config::from_env().region(region_provider).load().await;
       let client = Client::new(&shared_config);

       let table_name = "project5";
       let response = match insert_access_record(&client, table_name).await {
           Ok(_) => json!({"statusCode": 200, "body": json!({"message": "Record inserted successfully"}).to_string()}),
           Err(e) => json!({"statusCode": 500, "body": json!({"error": e.to_string()}).to_string()}),
       };

       Ok(response)
   }

   #[tokio::main]
   async fn main() -> Result<(), LambdaError> {
       lambda_runtime::run(service_fn(handler)).await?;
       Ok(())
   }

   ```
4. Build, test and deploy this lambda function with a role that has the permissions to access to DynamoDB and AWSLambda.
   ```
   cargo lambda build
   cargo lambda deploy
   ```
5. Add a trigger (API Gateway) to this lambda function.

## Screenshot Display

[Click here](https://5o66oazoel.execute-api.us-east-2.amazonaws.com/default/project5) to add an item to my table and view the return message.

![img](API.png)

In AWS console, we can see items are added correctly to table "project5".

![img](db.png)
